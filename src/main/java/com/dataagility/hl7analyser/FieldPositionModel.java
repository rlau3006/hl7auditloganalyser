package com.dataagility.hl7analyser;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.HashSet;
import java.util.Set;

public class FieldPositionModel {
    private int fieldPosition;
    private Set<FieldValueModel> fieldValueModelList = new HashSet<FieldValueModel>();

    public FieldPositionModel(int fieldPosition) {
        this.fieldPosition = fieldPosition;
    }

    public int getFieldPosition() {
        return fieldPosition;
    }

    public void setFieldPosition(int fieldPosition) {
        this.fieldPosition = fieldPosition;
    }

    public Set<FieldValueModel> getFieldValueModelList() {
        return fieldValueModelList;
    }

    public void addUniqueFieldValueModel(FieldValueModel model) {
        fieldValueModelList.add(model);
    }

    @Override
    public boolean equals(Object obj) {
        FieldPositionModel fieldPositionModel = (FieldPositionModel)obj;
        return this.fieldPosition==fieldPositionModel.getFieldPosition();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.fieldPosition).toHashCode();
    }

}
