package com.dataagility.hl7analyser;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class FieldValueModel {
    private String value;
    private int count;

    public FieldValueModel(String value) {
        this.value = value;
        this.count++;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.count++;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object obj) {
        FieldValueModel fieldValueModel = (FieldValueModel)obj;
        return this.value.compareTo(fieldValueModel.getValue()) == 0 ? true : false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.value).toHashCode();
    }
}
