package com.dataagility.hl7analyser;

public class AnalyserException extends Exception {
    public AnalyserException() {
    }

    public AnalyserException(String message) {
        super(message);
    }

    public AnalyserException(String message, Throwable cause) {
        super(message, cause);
    }

    public AnalyserException(Throwable cause) {
        super(cause);
    }
}
