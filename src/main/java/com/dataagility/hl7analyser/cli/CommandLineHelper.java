package com.dataagility.hl7analyser.cli;

import org.apache.commons.cli.*;

import java.util.Arrays;

import static java.lang.System.out;

public class CommandLineHelper {

    public static String FILE_OPTION = "file";
    public static String COUNT_OPTION = "new-file-count";
    public static String USAGE_OPTION = "usage";

    public static Options generateOptions()
    {
        final Option fileOption = Option.builder("f")
                .required(false)
                .longOpt(FILE_OPTION)
                .hasArg()
                .desc("File to be processed.")
                .build();

        final Option usageOption = Option.builder("?")
                .required(false)
                .hasArg(false)
                .longOpt(USAGE_OPTION)
                .desc("Usage message.")
                .build();

        final Options options = new Options();
        options.addOption(fileOption);
        options.addOption(usageOption);
        return options;
    }

    public static CommandLine generateCommandLine(
            final Options options, final String[] commandLineArguments)
    {
        final CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine commandLine = null;
        try
        {
            commandLine = cmdLineParser.parse(options, commandLineArguments);
        }
        catch (ParseException parseException)
        {
            out.println(
                    "ERROR: Unable to parse command-line arguments "
                            + Arrays.toString(commandLineArguments) + " due to: "
                            + parseException);
        }
        return commandLine;
    }

    public static void printUsage(final Options options)
    {
        final HelpFormatter formatter = new HelpFormatter();
        final String syntax = "HL7Analyser";
        out.println("\n=====");
        out.println("usage: java -jar hl7Analyser.jar [-?] [-f <input-file>] ");
        out.println("=====");
    }
}
