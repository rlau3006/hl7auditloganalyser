package com.dataagility.hl7analyser;

import com.dataagility.hl7analyser.cli.*;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.lang.System.*;

public class HL7Analyser {

    private static final char DELIMITER_CHAR = ',';
    private static final String TARGET_SEGMENT_NAME = "MSH";
    private static final int TARGET_FIELD_NUMBER = 3;
    private static String outputFilename;
    private static List<SegmentModel> segmentModelList = new ArrayList<SegmentModel>();


    public static void main(String[] args) {
        Options options = CommandLineHelper.generateOptions();
        CommandLine cmdline = CommandLineHelper.generateCommandLine(options, args);
        String inputFilename = cmdline.getOptionValue(CommandLineHelper.FILE_OPTION);
        File inputfile = new File(inputFilename);
        outputFilename = inputFilename + ".report.txt";
        File reportFile = new File(outputFilename);

        try {
            LineIterator lines = FileUtils.lineIterator(inputfile);
            while (lines.hasNext()) {
                String currentLine = lines.next();
                processMessageLine(currentLine);
//                printSingleFieldFromEveryLineOfTargetSegment(currentLine, TARGET_SEGMENT_NAME, TARGET_FIELD_NUMBER);

            }

            FileUtils.writeStringToFile(reportFile, outputReport(), (String)null);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (AnalyserException e) {
            out.println("Fatal issue during HL7 audit log file analysis...exiting now");
            e.printStackTrace();
            exit(1);
        }

    }

    static void processMessageLine(String line) throws AnalyserException {
        if (!isValidMessageLine(line)) {
            return;
        }

        //split the segment raw line into fields array
        String[] fieldsArray = StringUtils.split(line, '|');

        if( fieldsArray.length == 0 )
            throw new AnalyserException("bad segment line:" + line);

        //record the segment name
        String segmentName = fieldsArray[0];

        //go get the segmentModel - or create a new one if it doesn't exist
        SegmentModel currentSegmentModel = retreiveSegmentModel(segmentName);
        currentSegmentModel.setLineRaw(line);

        //now go through all the fields for this segment line and record all non-null values
        for (int fieldPosNum = 0; fieldPosNum < fieldsArray.length; fieldPosNum++) {
            FieldPositionModel currFieldPositionModel = new FieldPositionModel(fieldPosNum);

            if ( !currentSegmentModel.getFieldPositionModelList().isEmpty() && currentSegmentModel.getFieldPositionModelList().contains(currFieldPositionModel)) {
                //here we replace the temporary FieldPositionModel with the cached fieldPositionModel - it has the count of values
                currFieldPositionModel = currentSegmentModel.getFieldPositionModelList().get(fieldPosNum);
            } else {
                currentSegmentModel.getFieldPositionModelList().add(currFieldPositionModel);
            }

            if ( fieldPosNum == 0) {
                //we ignore fieldPosition 0 - this is always the segment name so not a valid field value
                continue;
            }

            //do this for all non-zero field positions
            String fieldValue = fieldsArray[fieldPosNum];
            if ( StringUtils.isBlank(fieldValue) )
                continue;

            FieldValueModel currFieldValueModel = new FieldValueModel(fieldValue);
            if ( currFieldPositionModel.getFieldValueModelList().contains(currFieldValueModel) ) {

                //find that FieldValueModel and add the value - thus incrementing the value count
                Iterator<FieldValueModel> iterator = currFieldPositionModel.getFieldValueModelList().iterator();
                while ( iterator.hasNext() ) {
                    FieldValueModel valueModel = iterator.next();
                    if (valueModel.equals(currFieldValueModel)) {
                        valueModel.setValue(currFieldValueModel.getValue());
                    }
                }
            } else {
                //this fieldvaluemodel isn't included in the Set of field values - so add it now
                currFieldPositionModel.getFieldValueModelList().add(currFieldValueModel);
            }
        }
    }

    static void printSingleFieldFromEveryLineOfTargetSegment(String line, String targetSegmentName, int targetFieldNumber) throws AnalyserException {
        if (!isValidMessageLine(line)) {
            return;
        }
        if (!line.startsWith(targetSegmentName))
            return;
        String[] fieldsArray = StringUtils.split(line, '|');
        System.out.println(fieldsArray[targetFieldNumber]);
    }

    static boolean isValidMessageLine(String line) {
        if (!(StringUtils.contains(line, "<<< Inbound <<<") || StringUtils.contains(line, ">>> Outbound >>>") || StringUtils.isBlank(line)))
            return true;
        else
            return false;
    }

    static SegmentModel retreiveSegmentModel(String segmentName) {
        Iterator<SegmentModel> iter = segmentModelList.iterator();
        while (iter.hasNext()) {
            SegmentModel currentSegmentModel = iter.next();
            if (currentSegmentModel.getSegmentName().compareTo(segmentName) == 0 )
                return currentSegmentModel;
        }
        SegmentModel uniqueSegmentModel = new SegmentModel(segmentName);
        segmentModelList.add(uniqueSegmentModel);
        return uniqueSegmentModel;
    }

    static String outputReport() {

        StringBuilder reportBuilder = new StringBuilder();
        Iterator<SegmentModel> segIter = segmentModelList.iterator();
        while(segIter.hasNext()) {
            SegmentModel segmentModel = segIter.next();
            StringBuilder segmentStringBuilder = new StringBuilder();
            String currentSegmentName = segmentModel.getSegmentName();

            Iterator<FieldPositionModel> iter = segmentModel.getFieldPositionModelList().iterator();
            while (iter.hasNext()) {
                FieldPositionModel fieldPositionModel = iter.next();
                int currFieldPos = fieldPositionModel.getFieldPosition();
                String fieldLabel = currentSegmentName + "." + currFieldPos;
                segmentStringBuilder.append(fieldLabel);
                segmentStringBuilder.append(":");

                Iterator<FieldValueModel> fieldValuesIter = fieldPositionModel.getFieldValueModelList().iterator();
                while (fieldValuesIter.hasNext()) {
                    FieldValueModel fieldValue = fieldValuesIter.next();
                    segmentStringBuilder.append(outputAllFieldValues(fieldValue));
                }
                segmentStringBuilder.append('\n');
            }
            reportBuilder.append(segmentStringBuilder.toString());
        }

        return reportBuilder.toString();
    }

    static String outputAllFieldValues(FieldValueModel model) {
        StringBuilder valuesBuilder = new StringBuilder();
        valuesBuilder.append("|value=\"");
        valuesBuilder.append(model.getValue());
        valuesBuilder.append("\"");
        valuesBuilder.append("|count=");
        valuesBuilder.append(model.getCount());
        return valuesBuilder.toString();
    }
}
