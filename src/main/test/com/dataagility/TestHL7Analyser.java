package com.dataagility;

import com.dataagility.hl7analyser.HL7Analyser;

public class TestHL7Analyser {

    public static void main(String[] args) {

        //path to filename is the root of HL7Analyser project area
//        String[] arguments = {"-f", "FromCSC_BHH_201902.log"};
//        String[] arguments = {"-f", "FromCSC_MAR_201902.log"};
//        String[] arguments = {"-f", "FromCSC_ANG_201902.log"};
//        String[] arguments = {"-f", "FromCSC_PJC_201902.log"};
//        String[] arguments = {"-f", "FromCSC_YRS_201902.log"};
        String[] arguments = {"-f", "testHl7Msg.log"};

        HL7Analyser.main(arguments);
    }
}
